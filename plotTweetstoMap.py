import sqlite3 as db

conn = db.connect('Dataset_2.db')
cur = conn.cursor()

arr_usernames = []
arr_lats, arr_lons = [], []
arr_followers = []

arr_f_usernames = []
arr_f_lats, arr_f_lons = [], []
arr_f_followers = []

arr_f_s_usernames = []
arr_f_s_followers = []
arr_f_s_index = []

false_value = "False"

cur.execute("SELECT DISTINCT username, location_coordinates, num_followers FROM Dataset10 WHERE location != ? AND location_coordinates != ?", (false_value, false_value))
all_rows = cur.fetchall()
for row in all_rows:
    arr_usernames.append(str(row[0]))
    arr_lats.append(str((row[1]).lstrip('(')).split(',')[0])
    arr_lons.append(str((row[1]).rstrip(')')).split(', ')[1])
    arr_followers.append(str(row[2]))

cur.execute("SELECT src_username, f_location_coordinates, f_num_followers FROM Followers10 WHERE f_location_coordinates != ? ORDER BY src_username ASC", (false_value,))
all_f_rows = cur.fetchall()
for f_row in all_f_rows:
    arr_f_usernames.append(str(f_row[0]))
    arr_f_lats.append(str((f_row[1]).lstrip('(')).split(',')[0])
    arr_f_lons.append(str((f_row[1]).rstrip(')')).split(', ')[1])
    arr_f_followers.append(str(f_row[2]))

print arr_f_usernames
print arr_f_lats
print arr_f_lons
print arr_f_followers

cur.execute("SELECT src_username, SUM(f_num_followers) FROM Followers10 WHERE f_location_coordinates != ? GROUP BY src_username ORDER BY src_username ASC", (false_value,))
all_f_s_rows = cur.fetchall()
for f_s_rows in all_f_s_rows:
    arr_f_s_usernames.append(str(f_s_rows[0]))
    arr_f_s_followers.append(str(f_s_rows[1]))
    arr_f_s_index.append(arr_f_usernames.count(f_s_rows[0]))

print arr_f_s_usernames
print arr_f_s_followers
print arr_f_s_index

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import random

def get_marker_color(followers):
    if followers < 1000:
        return ('go')
    elif followers < 2500:
        return ('yo')
    else:
        return ('ro')

colors = ['go','yo','ro']

eq_map = Basemap(projection='robin', resolution = 'l', area_thresh = 1000.0,
              lat_0=55.012153, lon_0=-3.446869)
eq_map.drawcoastlines()
eq_map.drawcountries()
eq_map.fillcontinents(color = 'mediumseagreen')
eq_map.drawmapboundary()
eq_map.drawmeridians(np.arange(0, 360, 30))
eq_map.drawparallels(np.arange(-90, 90, 30))

def getMap():
    min_marker_size = 5.0
    for lon, lat, fol in zip(arr_lons, arr_lats, arr_followers):
        x, y = eq_map(lon, lat)
        fsize = (float(fol) / 100) * min_marker_size
        marker_string = get_marker_color(fol)
        eq_map.plot(x, y, marker_string, markersize=min_marker_size)
    # title_string = "Tweets Spread based on Users' Locations"
    # plt.title(title_string)
    plt.show()

def getMapwithSize():
    min_marker_size = 1.0
    for lon, lat, fol in zip(arr_lons, arr_lats, arr_followers):
        x,y = eq_map(lon, lat)
        fsize = (float(fol)/100) * min_marker_size
        marker_string = get_marker_color(fol)
        eq_map.plot(x, y, marker_string, markersize=fsize ,alpha=0.3)
    # title_string = "How the Fake News Tweets Spread based on Number of Followers"
    # plt.title(title_string)
    plt.show()

def determineColor(name):
    try:
        if name == arr_f_s_usernames[0]:
            return 'indianred'
        elif name == arr_f_s_usernames[1]:
            return 'salmon'
        elif name == arr_f_s_usernames[2]:
            return 'chocolate'
        elif name == arr_f_s_usernames[3]:
            return 'burlywood'
        elif name == arr_f_s_usernames[4]:
            return 'wheat'
        elif name == arr_f_s_usernames[5]:
            return 'y'
        elif name == arr_f_s_usernames[6]:
            return 'powderblue'
        elif name == arr_f_s_usernames[7]:
            return 'lightskyblue'
        elif name == arr_f_s_usernames[8]:
            return 'lightsteelblue'
        elif name == arr_f_s_usernames[9]:
            return 'lavender'
        elif name == arr_f_s_usernames[10]:
            return 'lightpink'
        elif name == arr_f_s_usernames[11]:
            return 'paleturquoise'
        elif name == arr_f_s_usernames[12]:
            return 'moccasin'
        elif name == arr_f_s_usernames[13]:
            return 'bisque'
        elif name == arr_f_s_usernames[14]:
            return 'thistle'
        elif name == arr_f_s_usernames[15]:
            return 'mistyrose'
        elif name == arr_f_s_usernames[16]:
            return 'sandybrown'
        elif name == arr_f_s_usernames[17]:
            return 'darkturquoise'
        elif name == arr_f_s_usernames[18]:
            return 'plum'
        elif name == arr_f_s_usernames[19]:
            return 'gold'
        elif name not in arr_f_s_usernames:
            return 'pink'
        else:
            return 'yellow'
    except Exception:
        return 'green'


# for each user tweeted in tweets6, got 5 followerlist, API allows only til 20 each user tweeted
def getFollowerMap():
    min_marker_size_x = 1.0
    for lon, lat, fol in zip(arr_lons, arr_lats, arr_followers):
        x, y = eq_map(lon, lat)
        fsize = (float(fol) / 100) * min_marker_size_x
        marker_string = get_marker_color(fol)
        eq_map.plot(x, y, marker_string, markersize=min_marker_size_x)

    min_marker_size = 1.0
    for lon, lat, fol in zip(arr_lons, arr_lats, arr_followers):
        x, y = eq_map(lon, lat)
        fsize = (float(fol) / 100) * min_marker_size
        marker_string = get_marker_color(fol)
        eq_map.plot(x, y, marker_string, markersize=fsize, alpha=0.3)

    min_marker_size = 0.05
    for lon, lat, fol, uname in zip(arr_f_lons, arr_f_lats, arr_f_followers, arr_f_usernames):
        x, y = eq_map(lon, lat)
        fsize = (float(fol) / 100) * min_marker_size
        #marker_string = get_marker_color(fol)
        eq_map.plot(x, y, color=determineColor(uname), marker='o', markersize=fsize, alpha=0.8)
    # title_string = "Tweets Spread based on Users' Locations"
    # plt.title(title_string)
    plt.show()

# Plot tweets on the map (same marker size, just to show the location)
#getMap()

# Plot tweets with different size of marker to represent the number of followers and the spread by the overlaps
#getMapwithSize()

getFollowerMap()