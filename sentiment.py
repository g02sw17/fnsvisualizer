from textblob import TextBlob
from textblob.sentiments import NaiveBayesAnalyzer
import re

class SentimentAnalyzer(object):

    def get_polarity(self, tweet):
        sentiment = self.get_sentiment(tweet, "PatternAnalyzer")
        return sentiment.polarity

    def get_sentiment(self, tweet, analyzer):
        tweet = self.clean_tweet(tweet)
        if (analyzer == "PatternAnalyzer"):
            analysis = TextBlob(tweet)
            return analysis.sentiment
            # e.g. Sentiment(polarity=-0.8, subjectivity=0.9)
        elif (analyzer == "NaiveBayesAnalyzer"):
            analysis = TextBlob(tweet, analyzer=NaiveBayesAnalyzer())
            return analysis.sentiment
            # e.g. Sentiment(classification='neg', p_pos=0.332744, p_neg=0.6672555)
        else:
            print("Unsupported Analyzer! Use PatternAnalyzer or NaiveBayesAnalyzer.")
            return

    def clean_tweet(self, tweet):
        # Utility function to clean tweet text by removing links, special characters using simple regex statements.
        return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())

    def categorize_sentiments(self, values):
        # calculate amount of negative, neutral and positive tweets
        negative = 0
        neutral = 0
        positive = 0
        for s in values:
            if s < -0.0:
                negative += 1
            elif s > 0.0:
                positive += 1
            else:
                neutral += 1
        return negative, neutral, positive
