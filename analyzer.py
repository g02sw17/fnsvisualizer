from sentiment import SentimentAnalyzer
import sqlite3 as lite
import json
import sys

# number of the column that contains the tweet text
TWEET_COLUMN = 7

def run_sentiment_analysis(database, table, f):

    sentiment = SentimentAnalyzer()
    values = []

    # calculate sentiments for tweets from database
    con = lite.connect(database)
    with con:

        cur = con.cursor()
        cur.execute("SELECT * FROM %s" % table)

        while True:
            row = cur.fetchone()
            if row == None:
                break
            # print row[TWEET_COLUMN]
            polarity = sentiment.get_polarity(row[TWEET_COLUMN])
            values.append(polarity)

    # categorize sentiments for pie chart
    negative, neutral, positive = sentiment.categorize_sentiments(values)

    # format for pie chart:
    # [
    #   ["Polarity", "Number"],
    #   ["Negative", 26],
    # 	["Neutral", 89],
    # 	["Positive", 5]
    # ]
    f.write("data1.sentimentPieChart = ")
    f.write(json.dumps([["Polarity", "Number"],["Negative", negative],["Neutral", neutral],["Positive", positive]]))
    f.write(";\n\n")

    # format for histogram:
    # [
    #   ["Number"],
    #   [0.1],
    #   [-.05],
    #   [-0.5],
    #   [0.25]
	# ]
    f.write('data1.sentimentHistogramData = [\n\t["Number"],\n')
    for x in values:
        f.write("\t[%f],\n" % x)
    #f.write(json.dumps([["Number"],values], indent=2))
    f.write("];\n")

def run_analyses(database, table, output):
    f = open(output, 'w')
    f.write("var data1 = {};\n\n")

    sys.stdout.write('Sentiment Analysis: ')
    sys.stdout.flush()
    run_sentiment_analysis(database, table, f)
    sys.stdout.write('Done\n')
    sys.stdout.flush()

    # TODO: spread analysis

    # TODO: geographical analysis

    f.close()

def main():
    # default test values
    database = 'Dataset.db'
    table = 'Dataset1'
    output = 'sentiments.js'

    if len(sys.argv) > 2:
        database = sys.argv[1]
        table = sys.argv[2]

    if len(sys.argv) > 3:
        output = sys.argv[3]

    run_analyses(database, table, output)


if __name__ == "__main__":
    main()
