var data1 = {};

data1.numberOfExposedPeople = 123456;
data1.accumulatedExposure = [
	[0, 0],
	[1, 1000],
	[2. 1700]
];

data1.histogramExposureData = [
	["Day", "Number"],
	["Day 1", 1000],
	["Day 2", 700]
];

data1.sentimentPieChart = [
	          ['Sentiment', 'Percentage'],
	          ['Positive', 100],
	          ['Neutral', 30],
	          ['Negative', 50]
	        ];

data1.sentimentHistogramData = [
				["Number"],
		        [0.1],
		        [-.05],
		        [-0.5],
		        [0.25],
		        [0.25],
		        [0.15],
		        [0.5],
		        [-0.10],
		        [0.7],
		        [-0.8],
		        [0.6]
	      	];
