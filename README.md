# Fake News Spread Visualizer

This is a final project assignment for the Social Web 2017 course at VU Amsterdam. 
This project is developed by Group 2:
- Christian Degott
- Paul Noorland
- Felix Warners
- Anggarda Prameswari

This project will be build upon these milestones:
- Identify fake news URLs 
- Retrieve tweets with Twitter API
- Visualize total and per day of: number of people, geographical, and sentiment

For accessing the Twitter API we use tweepy.

Fakenews list: 
1. Did Paul Ryan Say Women Who Use Birth Control Are Committing Murder?
http://www.snopes.com/paul-ryan-birth-control-murder/

2. FBI Issues Warrant for Obama’s Arrest After Confirming Illegal Trump Tower Wiretap?
http://www.snopes.com/fbi-arrest-warrant-obama/

3. Man Pardoned by Obama Arrested for Murder?
http://www.snopes.com/man-pardoned-by-obama-arrested-murder/

4. Tommy Chong Death Hoax
http://www.snopes.com/false-tommy-chong-dead/

5. Did 122 Prisoners Released from Guantanamo Under President Obama Return to the Battlefield?
http://www.snopes.com/obama-prisoner-release/

7: http://thepoliticalinsider.com/wikileaks-confirms-hillary-sold-weapons-isis-drops-another-bombshell-breaking-news/ 

8: http://lagauchematuer.fr/2017/03/10/la-banque-europeenne-va-preter-100-millions-de-a-letat-afin-de-racheter-des-hotels-pour-y-loger-des-migrants/

9: http://www.breitbart.com/2016-presidential-race/2016/09/10/exposed-fbi-director-james-comeys-clinton-foundation-connection/

** Articles that have been used in map and spread analysis **
Tweets6.txt:
http://thepoliticalinsider.com/wikileaks-confirms-hillary-sold-weapons-isis-drops-another-bombshell-breaking-news/ 

Tweets9.txt:
http://www.breitbart.com/2016-presidential-race/2016/09/10/exposed-fbi-director-james-comeys-clinton-foundation-connection/

Tweets10.txt:
http://theliberal.ie/dont-worry-mr-bean-is-alive-internet-rumour-takes-off-that-rowan-atkinson-has-died/