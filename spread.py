import sqlite3 as lite
from dateutil.parser import parse

DATE_COLUMN = 0
FOLLOWER_COLUMN = 2

table = "Dataset10"
database = "Dataset_2.db"

dic = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0}
total = 0

con = lite.connect(database)

with con:

    cur = con.cursor()
    cur.execute("SELECT * FROM %s" % table)

    while True:
        row = cur.fetchone()
        if row == None:
            break
        b = parse(row[DATE_COLUMN])
        # works because the Twitter API only gives us the last 7 days
        dic[b.weekday()] += int(row[FOLLOWER_COLUMN])
        total += int(row[FOLLOWER_COLUMN])
        print row[DATE_COLUMN]
        # print row[FOLLOWER_COLUMN]

print dic
print total

# paste output into json by hand because we're running out of time
