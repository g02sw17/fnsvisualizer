from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import sqlite3 as lite
import sys
import time
import json
import apiConfig


con = lite.connect('tweets.db')

cur = con.cursor()

cur.execute("DROP TABLE IF EXISTS Tweets")
cur.execute("CREATE TABLE Tweets(username TEXT, tweet TEXT)")


#consumer key, consumer secret, access token, access secret.
ckey = apiConfig.ckey
csecret = apiConfig.csecret
atoken = apiConfig.atoken
asecret = apiConfig.asecret

class listener(StreamListener):

    def on_data(self, data):
        all_data = json.loads(data)

        tweet = all_data["text"]

        username = all_data["user"]["screen_name"]

        cur.execute("INSERT INTO Tweets (username, tweet) VALUES (?, ?)",
            (username, tweet))
        con.commit()

        print(username + ":\t\t" + tweet + "\n")

        return True

    def on_error(self, status):
        print status

auth = OAuthHandler(ckey, csecret)
auth.set_access_token(atoken, asecret)

twitterStream = Stream(auth, listener())
twitterStream.filter(track=["trump"])
