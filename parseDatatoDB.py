import json
from json import JSONDecoder
from functools import partial
import sqlite3 as lite
import json_lines
from pygeocoder import Geocoder
import jsonpickle
import tweepy
from pygeolib import GeocoderError

API_KEY = "jW18cAwx1BWmPSrYnagVDuxSO"
API_SECRET = "zVhC58FFYYTSVYYJZWfLNB0EiH6zr7G2ABVLOrDvzSHMIqYuTk"
atoken = "838150588949008385-JmOMl2EYxwvmmBdK7Plzl9XKfJl8sWH"
asecret = "tuOLMqtSTnN87Zh4crynFy1W2NAvgvZXAhVVXTuWtdqQI"

# API_KEY = 'DKjDxGqk1TMwRhMSalUO52K6M'
# API_SECRET = 'F7IgvlNKfK6qRPT7NprM32mSSO1IDzIykeNQzsSUMONepnUAN1'
# atoken = '38078478-KJWCsGC7RYDle2hDsQaP8mitAGby2pJAOxt3KBw2x'
# asecret = 'v6P2eUC0wXkPGz9ReHKkNVk0e9Vb2jP1bmWECEnW2Sw9L'

auth = tweepy.AppAuthHandler(API_KEY, API_SECRET)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

con = lite.connect('Dataset_2.db')
cur = con.cursor()

def createMultipleTable(x,y):
    for i in range (x,y):
        createQuery = "CREATE TABLE Dataset" + str(i) + " (created_at TEXT, username TEXT, num_followers TEXT, num_statuses TEXT, location TEXT, num_retweet TEXT, tweet TEXT, url TEXT, hashtag TEXT)"
        cur.execute(createQuery)

#cur.execute("CREATE TABLE Dataset10 (created_at TEXT, username TEXT, num_followers TEXT, num_statuses TEXT, location TEXT, location_coordinates TEXT, num_retweet TEXT, tweet TEXT, url TEXT, hashtag TEXT)")

def insertIntoDB(number):
    filename = "Tweets_" + str(number) + ".txt"
    with open(filename, 'r') as f:
        for data in json_lines.reader(f):
            created_at = data["created_at"]
            username = data["user"]["screen_name"]
            num_followers = data["user"]["followers_count"]
            num_statuses = data["user"]["statuses_count"]
            location = data["user"]["location"]
            num_retweet = data["retweet_count"]
            tweet = data["text"]

            url = []
            for i in range(len(data["entities"]["urls"])):
                url.append((data["entities"]["urls"][i]["expanded_url"]))
            urls = ",".join(url)

            hashtag = []
            for i in range(len(data["entities"]["hashtags"])):
                hashtag.append((data["entities"]["hashtags"][i]["text"]))
            hashtags = ','.join(hashtag)

            insertQuery = "INSERT INTO Dataset" + str(number) + "(created_at, username, num_followers, num_statuses, location, num_retweet, tweet, url, hashtag) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
            cur.execute(insertQuery,(created_at, username, num_followers, num_statuses, location, num_retweet, tweet, urls, hashtags))
            con.commit()

def validateAddres(query):
    try:
        address = Geocoder.geocode(query)
        if (address.coordinates is True) or (address.coordinates) is not '' or (address.coordinates is not None):
            return address.coordinates
        else:
            return False
    except GeocoderError:
        #print "The address entered could not be geocoded"
        return False

def tryInsertDBwithLocation():
    with open('Tweets_10.txt', 'r') as f:
        for data in json_lines.reader(f):
            created_at = data["created_at"]
            username = data["user"]["screen_name"]
            num_followers = data["user"]["followers_count"]
            num_statuses = data["user"]["statuses_count"]

            location = data["user"]["location"]
            location_coordinates = str(validateAddres(location))

            num_retweet = data["retweet_count"]
            tweet = data["text"]

            url = []
            for i in range(len(data["entities"]["urls"])):
                url.append((data["entities"]["urls"][i]["expanded_url"]))
            urls = ",".join(url)

            hashtag = []
            for i in range(len(data["entities"]["hashtags"])):
                hashtag.append((data["entities"]["hashtags"][i]["text"]))
            hashtags = ','.join(hashtag)

            insertQuery = "INSERT INTO Dataset10 (created_at, username, num_followers, num_statuses, location, location_coordinates, num_retweet, tweet, url, hashtag) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
            cur.execute(insertQuery,
                        (created_at, username, num_followers, num_statuses, location, location_coordinates, num_retweet, tweet, urls, hashtags))
            con.commit()

#tryInsertDBwithLocation()

arr_s_usernames = []
arr_s_followers = []

f_data = []
del f_data[:]
arr_f_usernames = []
arr_f_lats = []
arr_f_lons = []
arr_f_followers = []
arr_f_locations = []

cur.execute("CREATE TABLE Followers10 (src_username TEXT, f_num_followers TEXT, f_location TEXT, f_location_coordinates TEXT)")

# get list of followers by username
def getFlist():
    false_value = "False"
    cur.execute("SELECT DISTINCT username, num_followers FROM Dataset10 WHERE location != ? AND location_coordinates != ?",(false_value, false_value))
    all_rows = cur.fetchall()
    for row in all_rows:
        arr_s_usernames.append(str(row[0]))
        arr_s_followers.append(str(row[1]))
    print arr_s_usernames
    print arr_s_followers

    #print jsonpickle.encode(api.followers(screen_name = "delphine2bellev",count=5),unpicklable=False)

    for i in range(0,20):
        f_data.append(jsonpickle.encode(api.followers(screen_name =arr_s_usernames[i],count=5),unpicklable=False))
        i += 1

    for i in range(0,20):
        i_f_data = jsonpickle.decode(f_data[i])
        for j in range(0,5):
            tmp_location = i_f_data[j]["location"]
            tmp_num_f = i_f_data[j]["followers_count"]
            cur.execute("INSERT INTO Followers10 (src_username, f_num_followers, f_location, f_location_coordinates) VALUES (?, ?, ?, ?)", (arr_s_usernames[i], tmp_num_f, tmp_location, str(validateAddres(tmp_location))))
            j += 1
        i +=1

    con.commit()
getFlist()
